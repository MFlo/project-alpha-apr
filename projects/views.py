from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from .forms import ProjectForm


@login_required
def list_view(request):
    projects = Project.objects.filter(owner=request.user)
    if not projects:
        message = "You are not assigned to any projects"
    else:
        message = None
    context = {
        "projects": projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id, owner=request.user)
    context = {
        "project": project,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)


# In my view.  Single quotes is easier to use because you
# dont have to shift.  Exept when it comes to other languages.
# It might be an issue.  I don't know enough to be sure.  Oh well!
