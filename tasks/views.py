from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from .forms import TaskForm
from .models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    return render(request, "tasks/create_task.html", {"form": form})


@login_required
def my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    if not tasks:
        message = "You are not assigned to any projects"
    else:
        message = None
    return render(request, "tasks/my_tasks.html", {"tasks": tasks})


@require_POST
def update_task_status(request):
    for key in request.POST:
        if key.startswith("task_completed_"):
            task_id = key.split("_")[2]
            task = Task.objects.get(id=task_id)
            task.is_completed = key in request.POST
            task.save()
    return redirect("show_my_tasks")


# In my view.  The greatest super power would be hydrokinesis.
# It is universal.  It is life.
