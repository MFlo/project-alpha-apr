from django.urls import path
from .views import create_task, my_tasks, update_task_status

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", my_tasks, name="show_my_tasks"),
    path("update-task-status/", update_task_status, name="update_task_status"),
]
