from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, SignUpForm


# Create your views here.
def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(username=username, password=password)
                user.save()
                return redirect("list_projects")
            else:
                form.add_error("password_confirmation", "Passwords must match")
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "registration/signup.html", context)


# In my view, and according to some.  The future of coding and technical literacy may
# be as important as literacy is today.  In 2020 MIT neuroscientist found that interpreting code
# activates a general-purpose brain network, but not language-processing centers.
# either way the best way to learn is practice!
